// Loading the Libraries
const { User } = require("../models/users");
const auth = require("../middleware/auth");

const Joi = require("joi");
const passwordComplexity = require("joi-password-complexity");
const bcrypt = require("bcrypt");

const express = require("express");
const router = express.Router();

// Password Complexity Setting
const complexityOptions = {
  min: 5,
  max: 250,
  lowerCase: 1,
  upperCase: 1,
  numeric: 1,
  symbol: 1,
  requirementCount: 2,
};

// Signed-in User
router.get("/me", auth, async (req, res) => {
  const user = await User.findById(req.user._id).select("-password");
  res.send(user);
});

// User Registeration API
router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Find User by Email
  let user = await User.findOne({ email: req.body.email });
  if (!user)
    return res
      .status(400)
      .send("User with this email address is not registered!");

  // Check the User Password
  const validPassword = await bcrypt.compare(req.body.password, user.password);
  if (!validPassword) return res.status(400).send("Invalid Password!");

  // Generate Webtoken
  const token = user.generateAuthToken();
  res.send(token);
});

// Request Validation
function validate(req) {
  const schema = Joi.object({
    email: Joi.string().min(5).max(255).required().email(),
    // Complex Password
    password: passwordComplexity(complexityOptions),
  });
  return schema.validate(req);
}

// Exporting the Authentication Module
module.exports = router;
