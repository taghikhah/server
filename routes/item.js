// Loading the Libraries
const controller = require("../controllers/item");
const express = require("express");
const router = express.Router();

// Loading the Authorisation and Authentication Libraries
const auth = require("../middleware/auth");
const admin = require("../middleware/admin");

// API request: Create a new Item
router.post("/", auth, controller.create);

// API request: Update an existing Item
router.put("/:id", auth, controller.update);

// API request: Delete an existing Item (Only logged-in admin can delete a document via x-auth-token)
router.delete("/:id", [auth, admin], controller.delete);

// API request: list of all Items
router.get("/", auth, controller.list);

// API request: Get the maximum priority of the Items of a Poi
router.get("/priority/:id", auth, controller.max);

// API request: Find an Item by ID
router.get("/:id", auth, controller.find);

// Export the module
module.exports = router;
