// Loading Libraries
const controller = require("../controllers/poi");
const express = require("express");
const router = express.Router();

// Loading the Authorisation and Authentication Libraries
const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
//const owner = require("../middleware/owner");

// API request: Create a new Poi
router.post("/", auth, controller.create);

// API request: Update an existing Poi
router.put("/:id", auth, controller.update);

// API request: Delete an existing Poi (Only logged-in admin can delete a document via x-auth-token)
router.delete("/:id", [auth, admin], controller.delete);

// API request: List of all Pois
router.get("/", auth, controller.list);

// API request: Get the maximum priority of Pois
router.get("/priority", auth, controller.max);

// API request: Find a Poi by ID
router.get("/:id", auth, controller.find);

// Exporting the Module
module.exports = router;
