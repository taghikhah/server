// Loading Libraries
const controller = require("../controllers/users");
const express = require("express");
const router = express.Router();

// Loading the Authorisation and Authentication Libraries
const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const owner = require("../middleware/owner");

// API request: Register a new User
router.post("/", [auth, owner, admin], controller.register);

// API request: Find an existing User account
router.get("/:id", auth, controller.find);

// API request: Update an existing User account credentials by User
router.put("/:id", auth, controller.update);

// API request: Upgrade an existing User privileges by Owner
router.put("/upgrade/:id", [auth, owner], controller.upgrade);

// API request: Delete an existing User account
router.delete("/:id", [auth, owner, admin], controller.delete);

// API request: List of all Users
router.get("/", auth, controller.list);

// API request: Authenticate(login) an existing User
router.get("/me", auth, controller.login);

module.exports = router;
