// Loading the Libraries
const controller = require("../controllers/v1");
const express = require("express");
const router = express.Router();

// Loading the Authorisation and Authentication Libraries
const auth = require("../middleware/auth");

// API request: List of Pois (v1/pois)
router.get("/pois", auth, controller.pois);

// API request: Find a Poi with the given ID (v1/pois/:id)
router.get("/pois/:id", auth, controller.poi);

// API request: List of Categories (v1/categories)
router.get("/categories", auth, controller.categories);

// Exporting the Module
module.exports = router;
