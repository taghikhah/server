// Loading Libraries
const controller = require("../controllers/file");
const express = require("express");
const router = express.Router();

// Loading the Authorisation and Authentication Libraries
const auth = require("../middleware/auth");

// API request: Upload a new File
router.post("/", controller.upload);

// API request: Find an existing File
router.get("/:id", controller.download);

// API request: List of all Files
router.get("/", controller.list);

// API request: Delete an existing File by name
router.delete("/:id", auth, controller.remove);

// API request: Delete an existing File by name
router.delete("/directory/:id", auth, controller.clear);

// Exporting the Module
module.exports = router;
