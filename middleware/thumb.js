const md5 = require("md5");
const util = require("util");
const multer = require("multer");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const path = `${__basedir}/uploads`;
    const folder = req.header("x-folder-name");

    if (!folder)
      return res
        .status(403)
        .send("Can not upload the file! No folder name provided.");

    const destination = `${path}/${folder}/`;
    cb(null, destination);
  },
  filename: (req, file, cb) => {
    function fileNameHash(name) {
      const index = name.lastIndexOf(".");
      const uniqueName = md5(name);
      const filename = uniqueName + "-thumb" + name.slice(index);
      return filename.toLowerCase();
    }

    const filename = fileNameHash(file.originalname);
    cb(null, filename);
  },
});

const uploadThumb = multer({
  storage: storage,
}).single("file");

let uploadThumbMiddleware = util.promisify(uploadThumb);
module.exports = uploadThumbMiddleware;
