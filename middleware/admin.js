module.exports = function (req, res, next) {
  const isAuthorized = (role) => {
    if (role === "owner" || role === "admin") {
      return false;
    } else {
      return true;
    }
  };

  if (isAuthorized(req.user.role))
    return res.status(401).send("Access denied.");

  next();
};
