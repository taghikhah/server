// Loading the Libraries
const express = require("express");
const app = express();

// Logging Errors
require("./startup/logging");
// Modules
require("./startup/routes")(app);
// Database Connection
require("./startup/db")();
// Configuration
require("./startup/config")();
// Validation
require("./startup/validation")();
// Production
require("./startup/prod")(app);
// Server
require("./startup/port")(app);
