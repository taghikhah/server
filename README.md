# Histocaching II - RESTful API

Histocaching is an Android application using AR technology to detect historical buildings in Dresden.

## Installation

Use the node package manager [npm](https://docs.npmjs.com/downloading-and-installing-packages-locally) to restore installed packages from the package.json file of the app folder.

```
npm install
```

## Environment Variables

Before starting the application, you need to define the "Authorization Token Key" for authentication purposes, and the "Database URL" which contains the database username and password, in the environment variables of the server.

Windows:

```bash
set hcii_jwtPrivateKey=mySecureKey
```

```bash
set hcii_database_url=mongodb://localhost
```

Linux or other Unix-based systems:

```bash
export hcii_jwtPrivateKey=mySecureKey
```

```bash
export hcii_database_url=mongodb://localhost
```

PowerShell:

```bash
$env:hcii_jwtPrivateKey="mySecureKey"
```

```bash
$env:hcii_database_url="mongodb://localhost"
```

## Cofig File

Then you can navigate to "config" directory, and edit the values of properties in "default.json" file (e.g. "Port number" or "Database name") as you wish.
\*IMPORTANT: do not assign any value to "jwtPrivateKey", and "dataBaseUrl" properties in this file.

## Create a folder

In order to save the files(images) in the server you have to create an new directory(folder) in the app root folder with the name "uploads".

\*IMPORTANT: "uploads" folder must only consist of image folders of pois which will be automatically created when a new poi added to the database. Therefore, after creating the "uploads" folder, make sure that any kind of hidden files SHOULD NOT be found in this folder. (e.g. ".DS_Store" hidden files which are added automatically to all directories in MacOS)

```
mkdir server/uploads
```

## Run

Finally, to run our API application, just simply navigate to the app folder and run the following command.

```
node index.js
```

## Usage

API requests: they require an authentication header token with the name "x-auth-token". The value of this token must be a jsonwebtoken of an authenticated user.

```
GET {base_url:port}/v1/pois  #returns list of 'pois'

GET {base_url:port}/v1/pois/{id}  #returns 'poi' with the given id

GET {base_url:port}/v1/catalog  #returns list of 'poi items'
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
