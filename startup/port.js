// Loading the Libraries
const winston = require("winston");
const config = require("config");

// Define the Port number
const port = process.env.PORT || config.port;

module.exports = function (app) {
  // Starting the Server on the Port
  app.listen(port, () => winston.info(`Listening on Port ${port} ...`));
};
