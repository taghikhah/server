const express = require("express");
const poi = require("../routes/poi");
const item = require("../routes/item");
const category = require("../routes/category");
const v1 = require("../routes/v1");
const users = require("../routes/users");
const auth = require("../routes/auth");
const files = require("../routes/file");
const error = require("../middleware/error");
const cors = require("cors");

module.exports = function (app) {
  app.use(express.json());
  app.use(cors());
  app.use(express.static("uploads"));
  app.use("/api/users", users);
  app.use("/api/auth", auth);
  app.use("/api/files", files);
  app.use("/api/category", category);
  app.use("/api/poi", poi);
  app.use("/api/item", item);
  app.use("/api/v1", v1);
  app.use(error);
};

/*const corsOptions = {
  origin: "http://localhost:3000",
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200,
};*/
