const config = require("config");

// Check the Enviroment Varibales on Startup
module.exports = function () {
  if (!config.get("jwtPrivateKey")) {
    throw new Error("FATAL ERROR: jwtPrivateKey is not defined.");
  }
};

// Set the base url
global.__basedir = __dirname.substring(0, __dirname.length - 8);
