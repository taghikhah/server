const winston = require("winston");
const mongoose = require("mongoose");
const config = require("config");

// Loading Datase Config
const database = `${config.get("dataBaseUrl")}/${config.database_name}`;

module.exports = function () {
  mongoose
    .connect(database, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    })
    .then(() => winston.info("Connected to the Database ..."));
};
