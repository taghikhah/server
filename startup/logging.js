const winston = require("winston");
require("winston-mongodb");
require("express-async-errors");

const logger = winston.createLogger({
  level: "info",
  format: winston.format.json(),
  defaultMeta: { service: "user-service" },
  transports: [
    // Write all logs with level 'error' and below to the errors.log
    new winston.transports.File({
      filename: "./logs/errors.log",
      level: "error",
    }),

    // Write all logs with level 'error' and below to the database in log collection
    new winston.transports.MongoDB({
      db: "mongodb://localhost/histo",
      options: { useNewUrlParser: true, useUnifiedTopology: true },
      level: "error",
      // storeHost: true,
      // collection: "log",
      capped: true,
    }),

    // Write all logs with level 'info' and below to the infos.log
  ],
  exceptionHandlers: [
    // Write all logs of uncaughtException events to the exceptions.log
    new winston.transports.File({ filename: "./logs/exceptions.log" }),
    // Log uncaughtException events to the console
    new winston.transports.Console({
      level: "info",
      format: winston.format.combine(
        winston.format.colorize({ all: true }),
        winston.format.simple()
      ),
    }),
  ],
});

// If we're not in production then log to the console
if (process.env.NODE_ENV !== "production") {
  // logger.add(new winston.transports.Console({ format: winston.format.colorize( {all: true}) }));
  logger.add(
    new winston.transports.Console({ format: winston.format.simple() })
  );
}

winston.add(logger);

/*
module.exports = function() {
  winston.exceptions.handle(
    new winston.transports.Console({ colorize: true, prettyPrint: true }),
    new winston.transports.File({ filename: 'uncaughtExceptions.log' }));
  
  process.on('unhandledRejection', (ex) => {
    throw ex;
  });
}
*/
