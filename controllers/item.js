// Loading the Libraries
const _ = require("lodash");
const { Item, validate } = require("../models/item");
const { Poi } = require("../models/poi");

// Create a new Item
exports.create = async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Validatetion of the given Poi ID
  const poi_id = await Poi.findById(req.body.poi_id);
  if (!poi_id) return res.status(400).send("Invalid poi ID.");

  // Find a category by their combination of captions
  let item = await Item.findOne(
    _.pick(req.body, ["caption_de"]) || _.pick(req.body, ["caption_en"])
  );
  if (item) return res.status(400).send("Item already exists.");

  // Finding the maximum priority of the Items of a Poi
  let max = 0;

  let query = await Item.find({ poi_id: req.body.poi_id })
    .limit(1)
    .sort({ priority: -1 })
    .select({ priority: 1 });

  if (query[0]) {
    max = query[0].priority;
  }

  // Save the new Item
  item = new Item({
    poi_id: {
      _id: poi_id._id,
    },
    priority: ++max,
    description_de: req.body.description_de,
    description_en: req.body.description_en,
    caption_de: req.body.caption_de,
    caption_en: req.body.caption_en,
    filename: req.body.filename,
    image_aspect_ratio: req.body.image_aspect_ratio,
  });
  item = await item.save();

  // Updating "updated_at" property of the correlated Poi
  const poi = await Poi.findByIdAndUpdate(
    item.poi_id,
    { updated_at: item.updated_at },
    { new: true }
  );

  res.send(item);
};

// Update an existing Item
exports.update = async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // ID Validation of the correlated Poi
  const poi_id = await Poi.findById(req.body.poi_id);
  if (!poi_id) return res.status(400).send("Invalid poi ID.");

  // Findind the current Priority of the given ID
  let item = await Item.findById(req.params.id);

  // Finding the maximum priority of the Items of a Poi
  let max = 0;

  let query = await Item.find({ poi_id: req.body.poi_id })
    .limit(1)
    .sort({ priority: -1 })
    .select({ priority: 1 });

  if (query[0]) {
    max = query[0].priority;
  }

  // Priority Validation
  if (req.body.priority > max) {
    return res
      .status(404)
      .send(
        `Invalid Priority. Maximum possible value of the Priority is ${max}!`
      );
  } else if (req.body.priority <= 0) {
    return res
      .status(404)
      .send("Invalid Priority. Minimum possible value of the Priority is 1!");
  }

  // Updating the Priority of other documents
  if (req.body.priority < item.priority) {
    await Item.updateMany(
      {
        poi_id: req.body.poi_id,
        priority: { $lt: item.priority, $gte: req.body.priority },
      },
      {
        $inc: { priority: 1 },
      }
    );
  } else if (req.body.priority > item.priority) {
    await Item.updateMany(
      {
        poi_id: req.body.poi_id,
        priority: { $gte: item.priority, $lte: req.body.priority },
      },
      {
        $inc: { priority: -1 },
      }
    );
  }

  // Update the Item with the given ID
  item = await Item.findByIdAndUpdate(
    req.params.id,
    {
      poi_id: req.body.poi_id,
      priority: req.body.priority,
      description_de: req.body.description_de,
      description_en: req.body.description_en,
      caption_de: req.body.caption_de,
      caption_en: req.body.caption_en,
      filename: req.body.filename,
      image_aspect_ratio: req.body.image_aspect_ratio,
    },
    { new: true }
  );

  // Updating "updated_at" property of the correlated Poi
  const poi = await Poi.findByIdAndUpdate(
    item.poi_id,
    { updated_at: item.updated_at },
    { new: true }
  );

  // Given ID Validation
  if (!item)
    return res.status(404).send("Item with the given ID was not found.");

  res.send(item);
};

// Delete an existing Item
exports.delete = async (req, res) => {
  // Findind the current Priority of the given ID
  let item = await Item.findById(req.params.id);

  // Finding the maximum priority of the Items of a Poi
  let max = 0;

  let query = await Item.find({ poi_id: item.poi_id })
    .limit(1)
    .sort({ priority: -1 })
    .select({ priority: 1 });

  if (query[0]) {
    max = query[0].priority;
  }

  // Change the Priority of other documents
  if (item.priority < max) {
    await Item.updateMany(
      {
        poi_id: item.poi_id,
        priority: { $gte: item.priority },
      },
      {
        $inc: { priority: -1 },
      }
    );
  }

  await Item.findByIdAndRemove(req.params.id);

  if (!item)
    return res.status(404).send("Item with the given ID was not found.");

  res.send(item);
};

// List of all Items
exports.list = async (req, res) => {
  const items = await Item.find().sort("priority");
  res.send(items);
};

// Maximum priority of the Items of a Poi
exports.max = async (req, res) => {
  const item = await Item.find({ poi_id: req.params.id })
    .limit(1)
    .sort({ priority: -1 })
    .select({ priority: 1 });

  res.send(item);
};

// Find an Item by ID
exports.find = async (req, res) => {
  const item = await Item.findById(req.params.id);

  // Given ID Validation
  if (!item)
    return res.status(404).send("Item with the given ID was not found.");

  res.send(item);
};
