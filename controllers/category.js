// Loading Libraries
const _ = require("lodash");
const { Category, validate } = require("../models/category");

// Create a new Category
exports.create = async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Find a category by their combination of English name and Deutsch name
  let category = await Category.findOne(
    _.pick(req.body, ["name_de"]) || _.pick(req.body, ["name_en"])
  );
  if (category) return res.status(400).send("Category already exists.");

  // Finding the maximum of Priorities
  let max = 0;

  let query = await Category.find()
    .limit(1)
    .sort({ priority: -1 })
    .select({ priority: 1 });

  if (query[0]) {
    max = query[0].priority;
  }

  // Save the new category
  category = new Category({
    priority: ++max,
    name_de: req.body.name_de,
    name_en: req.body.name_en,
  });
  // category = new Category(_.pick(req.body, ['name_de', 'name_en']));
  category = await category.save();

  res.send(category);
};

// Update an existing Category
exports.update = async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Find the current Priority of the given ID
  let category = await Category.findById(req.params.id);

  // Finding the maximum of Priorities
  let max = 0;

  let query = await Category.find()
    .limit(1)
    .sort({ priority: -1 })
    .select({ priority: 1 });

  if (query[0]) {
    max = query[0].priority;
  }

  // Priority Validation
  if (req.body.priority > max) {
    return res
      .status(404)
      .send(
        `Invalid Priority. Maximum possible value of the Priority is ${max}!`
      );
  } else if (req.body.priority <= 0) {
    return res
      .status(404)
      .send("Invalid Priority. Minimum possible value of the Priority is 1!");
  }

  // Change the Priority of documents
  if (req.body.priority < category.priority) {
    await Category.updateMany(
      {
        priority: { $lt: category.priority, $gte: req.body.priority },
      },
      {
        $inc: { priority: 1 },
      }
    );
  } else if (req.body.priority > category.priority) {
    await Category.updateMany(
      {
        priority: { $gte: category.priority, $lte: req.body.priority },
      },
      {
        $inc: { priority: -1 },
      }
    );
  }

  // Update the Category with the given ID
  category = await Category.findByIdAndUpdate(
    req.params.id,
    _.pick(req.body, ["name_de", "name_en", "priority"]),
    { new: true }
  );

  // Given ID Validation
  if (!category)
    return res.status(404).send("Category with the given ID was not found.");

  res.send(category);
};

// Delete an existing category
exports.delete = async (req, res) => {
  const category = await Category.findByIdAndRemove(req.params.id);

  // Change the Priority of other documents
  await Category.updateMany(
    {
      priority: { $gte: category.priority },
    },
    {
      $inc: { priority: -1 },
    }
  );

  if (!category)
    return res.status(404).send("Category with the given ID was not found.");

  res.send(category);
};

// List of all Categories
exports.list = async (req, res) => {
  const categories = await Category.find().sort("priority");

  res.send(categories);
};

// Maximum priority of Categories
exports.max = async (req, res) => {
  const category = await Category.find()
    .limit(1)
    .sort({ priority: -1 })
    .select({ priority: 1 });

  res.send(category);
};

// Find a Category by ID
exports.find = async (req, res) => {
  const category = await Category.findById(req.params.id);

  // Given ID Validation
  if (!category)
    return res.status(404).send("Category with the given ID was not found.");

  res.send(category);
};
