// Loading the Libraries
const config = require("config");
const { Poi } = require("../models/poi");
const { Item } = require("../models/item");
const { Category } = require("../models/category");

// List of Pois (v1/pois)
exports.pois = async (req, res) => {
  // Get all Pois
  const pois = await Poi.find().select({
    _id: 1,
    has_histocache_location: 1,
    has_viewpoint_location: 1,
    lat: 1,
    long: 1,
    viewpoint_lat: 1,
    viewpoint_long: 1,
  });

  // Get the time of last updated Poi
  const lastPoiUpdateTime = await Poi.find()
    .limit(1)
    .sort({ updated_at: -1 })
    .select({ updated_at: 1, _id: 0 });

  // Define and Validate the API response Object
  const result = { data: pois, meta: lastPoiUpdateTime[0] };
  if (!result) return res.status(404).send("No Pois found in the Database!");

  if (req.query) {
    if (result.meta.updated_at.toISOString() === req.query.updated_at) {
      res.status(204).send("No Content!");
    } else {
      res.send(result);
    }
  } else {
    res.send(result);
  }
};

// Find a Poi with the given ID (v1/pois/:id)
exports.poi = async (req, res) => {
  // Get and Validate the Poi with the given ID
  const poi = await Poi.findById(req.params.id);
  if (!poi) return res.status(404).send("Poi with the given ID was not found.");

  // Get correlated Category to each Poi
  const category = await Category.find({ _id: poi.category_id }).select({
    name_de: 1,
    name_en: 1,
    _id: 0,
  });

  // Get correlated Items to each Poi
  const allItems = await Item.find({ poi_id: req.params.id }).select({
    filename: 1,
    description_de: 1,
    description_en: 1,
    caption_de: 1,
    caption_en: 1,
    image_aspect_ratio: 1,
    _id: 0,
  });

  // Get the image rul of each Item
  const items = allItems.map(
    ({
      filename: image_url,
      description_en,
      description_de,
      caption_en,
      caption_de,
      image_aspect_ratio,
    }) => ({
      image_url: `${config.image_path}${req.params.id}/${image_url}`,
      description_en,
      description_de,
      caption_en,
      caption_de,
      image_aspect_ratio,
    })
  );

  let data = {
    category: category[0],
    title_en: poi.title_en,
    title_de: poi.title_de,
    description_en: poi.description_en,
    description_de: poi.description_de,
    caption_en: poi.caption_en,
    caption_de: poi.caption_de,
    add_info_url: poi.add_info_url,
    is_displayed_on_table: poi.is_displayed_on_table,
    image_url: `${config.image_path}${poi._id}/${poi.filename}`,
    image_aspect_ratio: poi.image_aspect_ratio,
    has_histocache_location: poi.has_histocache_location,
    has_viewpoint_location: poi.has_viewpoint_location,
    viewpoint_image_url: `${config.image_path}${poi._id}/${poi.viewpoint_filename}`,
    viewpoint_image_aspect_ratio: poi.viewpoint_image_aspect_ratio,
    viewpoint_image_height: poi.viewpoint_image_height,
    viewpoint_image_offset: poi.viewpoint_image_offset,
    viewpoint_image_vertical_offset: poi.viewpoint_image_vertical_offset,
    documents: items,
    updated_at: poi.updated_at,
  };

  // Define and Validate the API response Object
  const result = { data: data };
  if (!result)
    return res.status(404).send("Poi with the given ID was not found.");

  if (req.query) {
    if (result.data.updated_at.toISOString() === req.query.updated_at) {
      res.status(204).send("No Content!");
    } else {
      res.send(result);
    }
  } else {
    res.send(result);
  }
};

// List of Categories (v1/categories)
exports.categories = async (req, res) => {
  // Get all Categories
  const categories = await Category.find();

  // Get correlated Pois to each Category
  let category = [];
  for (let j = 0; j < categories.length; j++) {
    category.push({
      name_en: categories[j].name_en,
      name_de: categories[j].name_de,
      pois: await Poi.find({ category_id: categories[j]._id }).select({
        _id: 1,
        title_en: 1,
        title_de: 1,
      }),
    });
  }

  // Get the time of last updated Poi
  const lastPoiUpdateTime = await Poi.find()
    .limit(1)
    .sort({ updated_at: -1 })
    .select({ updated_at: 1, _id: 0 });

  // Get the time of last updated Category
  const lastCategoryUpdateTime = await Category.find()
    .limit(1)
    .sort({ updated_at: -1 })
    .select({ updated_at: 1, _id: 0 });

  // Compare the last updates
  const lastUpdateTime =
    lastPoiUpdateTime[0].updated_at > lastCategoryUpdateTime[0].updated_at
      ? lastPoiUpdateTime[0]
      : lastCategoryUpdateTime[0];

  // Define and Validate the API response Object
  const result = { data: category, meta: lastUpdateTime };
  if (!result)
    return res.status(404).send("No Category found in the Database!");

  if (req.query) {
    if (result.meta.updated_at.toISOString() === req.query.updated_at) {
      res.status(204).send("No Content!");
    } else {
      res.send(result);
    }
  } else {
    res.send(result);
  }
};
