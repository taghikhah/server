// Loading Libraries
const _ = require("lodash");
const bcrypt = require("bcrypt");
const { User, validate } = require("../models/users");

// Register a new User
exports.register = async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Find users by a property
  let user = await User.findOne({ email: req.body.email });
  if (user) return res.status(400).send("User is already registered.");

  // Using lodash for response of the request
  user = new User(_.pick(req.body, ["name", "email", "password", "role"]));

  // Hash the Password
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  // Save the User to the Database.
  await user.save();

  // Generate Webtoken
  const token = user.generateAuthToken();
  res
    .header("x-auth-token", token)
    .header("access-control-expose-headers", "x-auth-token")
    .send(_.pick(user, ["_id", "name", "email", "role", "created_at"]));
};

// Update an existing User account credentials by User
exports.update = async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Using lodash for response of the request
  const user = await User.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      role: req.body.role,
    },
    { new: true }
  );

  // Hash the Password
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  // Save the User to the Database.
  await user.save();

  if (!user)
    return res
      .status(404)
      .send("User account with the given ID was not found.");

  // Generate Webtoken
  const token = user.generateAuthToken();
  res
    .header("x-auth-token", token)
    .header("access-control-expose-headers", "x-auth-token")
    .send(_.pick(user, ["_id", "name", "email", "role", "updated_at"]));
};

// Upgrade an existing User privileges by Owner
exports.upgrade = async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const user = await User.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      role: req.body.role,
    },
    { new: true }
  );

  // Hash the Password
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  // Save the User to the Database.
  await user.save();

  if (!user)
    return res
      .status(404)
      .send("User account with the given ID was not found.");

  // Generate Webtoken
  const token = user.generateAuthToken();
  res
    .header("x-auth-token", token)
    .header("access-control-expose-headers", "x-auth-token")
    .send(_.pick(user, ["_id", "name", "email", "role", "updated_at"]));
};

// List of all Users
exports.list = async (req, res) => {
  const users = await User.find().sort("name");

  res.send(users);
};

// Delete an existing User account
exports.delete = async (req, res) => {
  const userRole = await User.findOne(req.params.id.role);
  if (userRole === "owner")
    return res.status(400).send("Owner account can not be deleted.");

  const user = await User.findByIdAndRemove(req.params.id);

  if (!user)
    return res
      .status(404)
      .send("User account with the given ID was not found.");

  res.send(user);
};

// Authenticate(login) an existing User
exports.login = async (req, res) => {
  const user = await User.findById(req.user._id).select("-password");
  /* 
    - Using auth module as the middleware Handler (Access denied if no token provided)
    - instead of passing :id as the endpoint we get it from the jsonwebtoken 
    - exclude password property
    */
  res.send(user);
};

// Find a User by ID
exports.find = async (req, res) => {
  const user = await User.findById(req.params.id);

  if (!user)
    return res.status(404).send("Poi with the given ID was not found.");

  res.send(user);
};
