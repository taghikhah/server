// Loading Libraries
const _ = require("lodash");
const { Poi, validate } = require("../models/poi");
const { Category } = require("../models/category");

// Create a new Poi
exports.create = async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Validatetion of the given Category ID
  const category_id = await Category.findById(req.body.category_id);
  if (!category_id) return res.status(400).send("Invalid category ID.");

  // Find a Poi by their combination of titles
  let poi = await Poi.findOne(_.pick(req.body, ["title_en", "title_de"]));
  if (poi) return res.status(400).send("This poi already exists.");

  // Finding the maximum of Priorities
  let max = 0;

  let query = await Poi.find()
    .limit(1)
    .sort({ priority: -1 })
    .select({ priority: 1 });

  if (query[0]) {
    max = query[0].priority;
  }

  // Save the new Category
  poi = new Poi({
    priority: ++max,
    category_id: {
      _id: category_id._id,
    },
    title_en: req.body.title_en,
    title_de: req.body.title_de,
    description_en: req.body.description_en,
    description_de: req.body.description_de,
    caption_en: req.body.caption_en,
    caption_de: req.body.caption_de,
    add_info_url: req.body.add_info_url,
    is_displayed_on_table: req.body.is_displayed_on_table,
    has_histocache_location: req.body.has_histocache_location,
    lat: req.body.lat,
    long: req.body.long,
    filename: req.body.filename,
    image_aspect_ratio: req.body.image_aspect_ratio,
    has_viewpoint_location: req.body.has_viewpoint_location,
    viewpoint_lat: req.body.viewpoint_lat,
    viewpoint_long: req.body.viewpoint_long,
    viewpoint_filename: req.body.viewpoint_filename,
    viewpoint_image_aspect_ratio: req.body.viewpoint_image_aspect_ratio,
    viewpoint_image_height: req.body.viewpoint_image_height,
    viewpoint_image_offset: req.body.viewpoint_image_offset,
    viewpoint_image_vertical_offset: req.body.viewpoint_image_vertical_offset,
  });
  poi = await poi.save();

  res.send(poi);
};

// Update an existing Poi
exports.update = async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Validatetion of the given Category ID
  const category_id = await Category.findById(req.body.category_id);
  if (!category_id) return res.status(400).send("Invalid category ID.");

  // Find the current Priority of the given ID
  let poi = await Poi.findById(req.params.id);

  // Finding the maximum of Priorities
  let max = 0;

  let query = await Poi.find()
    .limit(1)
    .sort({ priority: -1 })
    .select({ priority: 1 });

  if (query[0]) {
    max = query[0].priority;
  }

  // Priority Validation
  if (req.body.priority > max) {
    return res
      .status(404)
      .send(
        `Invalid Priority. Maximum possible value of the Priority is ${max}!`
      );
  } else if (req.body.priority <= 0) {
    return res
      .status(404)
      .send("Invalid Priority. Minimum possible value of the Priority is 1!");
  }

  // Update the Priority of other documents in case of modification
  if (req.body.priority < poi.priority) {
    await Poi.updateMany(
      {
        priority: { $lt: poi.priority, $gte: req.body.priority },
      },
      {
        $inc: { priority: 1 },
      }
    );
  } else if (req.body.priority > poi.priority) {
    await Poi.updateMany(
      {
        priority: { $gte: poi.priority, $lte: req.body.priority },
      },
      {
        $inc: { priority: -1 },
      }
    );
  }

  poi = await Poi.findByIdAndUpdate(
    req.params.id,
    {
      priority: req.body.priority,
      category_id: req.body.category_id,
      title_en: req.body.title_en,
      title_de: req.body.title_de,
      description_en: req.body.description_en,
      description_de: req.body.description_de,
      caption_en: req.body.caption_en,
      caption_de: req.body.caption_de,
      add_info_url: req.body.add_info_url,
      is_displayed_on_table: req.body.is_displayed_on_table,
      has_histocache_location: req.body.has_histocache_location,
      lat: req.body.lat,
      long: req.body.long,
      filename: req.body.filename,
      image_aspect_ratio: req.body.image_aspect_ratio,
      has_viewpoint_location: req.body.has_viewpoint_location,
      viewpoint_lat: req.body.viewpoint_lat,
      viewpoint_long: req.body.viewpoint_long,
      viewpoint_filename: req.body.viewpoint_filename,
      viewpoint_image_aspect_ratio: req.body.viewpoint_image_aspect_ratio,
      viewpoint_image_height: req.body.viewpoint_image_height,
      viewpoint_image_offset: req.body.viewpoint_image_offset,
      viewpoint_image_vertical_offset: req.body.viewpoint_image_vertical_offset,
    },
    { new: true }
  );

  // Given ID Validation
  if (!poi) return res.status(404).send("Poi with the given ID was not found.");

  res.send(poi);
};

// Delete an existing Poi (Only logged-in admin can delete a document via x-auth-token)
exports.delete = async (req, res) => {
  const poi = await Poi.findByIdAndRemove(req.params.id);

  // Change the Priority of other documents
  await Poi.updateMany(
    {
      priority: { $gte: poi.priority },
    },
    {
      $inc: { priority: -1 },
    }
  );

  if (!poi) return res.status(404).send("Poi with the given ID was not found.");

  res.send(poi);
};

// List of all Pois
exports.list = async (req, res) => {
  const pois = await Poi.find().sort("priority");
  res.send(pois);
};

// Maximum priority of Pois
exports.max = async (req, res) => {
  const poi = await Poi.find()
    .limit(1)
    .sort({ priority: -1 })
    .select({ priority: 1 });

  res.send(poi);
};

// Find a Poi by ID
exports.find = async (req, res) => {
  const poi = await Poi.findById(req.params.id);

  if (!poi) return res.status(404).send("Poi with the given ID was not found.");

  res.send(poi);
};
