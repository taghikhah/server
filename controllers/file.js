// Loading Libraries
const fs = require("fs");
const sharp = require("sharp");
const { unlink, readdir } = require("fs").promises;
const path = require("path");
const config = require("config");
const uploadFile = require("../middleware/upload");
const uploadThumb = require("../middleware/thumb");
const getFiles = require("../middleware/walk");

// API request upload a file
const upload = async (req, res) => {
  // Save the main Image
  uploadFile(req, res);

  try {
    // Save image as thumb
    await uploadThumb(req, res);

    // Resize the thumb
    sharp(req.file.path)
      .resize({ width: 200 })
      .toBuffer()
      .then((data) => {
        fs.writeFile(
          `${req.file.destination}${req.file.filename}`,
          data,
          function (err) {
            if (err) {
              return res.status(500).send({
                message: `Could not upload the file: 
                ${req.file.originalname}. 
                ${err}`,
              });
            }
          }
        );
      });

    if (req.file == undefined) {
      return res.status(400).send({ message: "Please upload a file!" });
    }

    res.status(200).send({
      message: `Successfully uploaded the file:
      ${req.file.originalname}.`,
    });
  } catch (err) {
    res.status(500).send({
      message: `Could not upload the file: 
      ${req.file.originalname}. 
      ${err}`,
    });
  }
};

// API request get list of all fies
const list = async (req, res) => {
  const directoryPath = __basedir + "/uploads/";

  await getFiles(directoryPath)
    .then((files) => {
      let fileInfos = [];

      files.forEach((file) => {
        fileInfos.push({
          name: path.basename(file),
          url: `${config.image_path}${path.basename(
            path.dirname(file)
          )}/${path.basename(file)}`,
          parent: path.basename(path.dirname(file)),
        });
      });
      res.status(200).send(fileInfos);
    })
    .catch((err) => {
      if (err) {
        res.status(500).send({
          message: "Unable to scan files!",
          err,
        });
      }
    });
};

// API request: get a file by name
const download = async (req, res) => {
  const file_name = req.params.id;

  const directoryPath = __basedir + "/uploads/";

  await getFiles(directoryPath)
    .then((files) => {
      const allFiles = [];

      files.forEach((file) => {
        allFiles.push({
          name: path.basename(file),
          url: `${config.image_path}${path.basename(
            path.dirname(file)
          )}/${path.basename(file)}`,
          poi: path.basename(path.dirname(file)),
        });
      });

      const result = allFiles.find((f) => f.name === file_name);
      res.status(200).send(result);
    })
    .catch((err) => {
      if (err) {
        res.status(500).send({
          message: `Could not download the file:
        ${file_name}.`,
          error: err,
        });
      }
    });
};

// API request: delete a file by name
const remove = async (req, res) => {
  const file_name = req.params.id;
  const directoryPath = __basedir + "/uploads/";

  await getFiles(directoryPath)
    .then((files) => {
      const allFiles = [];

      files.forEach((file) => {
        allFiles.push({
          name: path.basename(file),
          path: file,
        });
      });

      // Main Image
      const result = allFiles.find((f) => f.name === file_name);

      // Thumb Image
      const index = result.path.lastIndexOf(".");
      const name = result.path.substring(0, result.path.lastIndexOf("."));
      const thumb = name + "-thumb" + result.path.slice(index);

      // Delete thumb image
      unlink(thumb);

      // Delete main image
      unlink(result.path);

      res.status(200).send({
        message: `Successfully deleted the file: 
        ${file_name}`,
      });
    })
    .catch((err) => {
      if (err) {
        res.status(500).send({
          message: `File with given name does not exist. 
          ${err}`,
        });
      }
    });
};

// API request: delete a directory by name
const clear = async (req, res) => {
  const directory = req.params.id;
  const directoryPath = __basedir + "/uploads";

  // Check if the directory exists
  const dirents = await readdir(directoryPath, { withFileTypes: true });
  const result = dirents.find((d) => d.name === directory);

  if (!result)
    return res.status(500).send("Directory with given name does not exist.");

  // Delete if the directory exists
  await fs.rmdirSync(`${directoryPath}/${directory}`, {
    recursive: true,
    force: true,
  });

  res.status(200).send({
    message: "Histocache directory deleted successfully.",
  });
};

module.exports = {
  upload,
  list,
  download,
  remove,
  clear,
};
