const config = require("config");
const jwt = require("jsonwebtoken");
const Joi = require("joi");
const passwordComplexity = require("joi-password-complexity");
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      minlength: 3,
      maxlength: 50,
    },
    email: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 255,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      minlength: 8,
      maxlength: 1024,
    },
    isAdmin: Boolean,
    role: {
      type: String,
      enum: ["editor", "admin", "owner"],
      default: "editor",
    },
  },
  {
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at",
    },
  }
);

// Generate Token
userSchema.methods.generateAuthToken = function () {
  const token = jwt.sign(
    { _id: this._id, name: this.name, role: this.role },
    config.get("jwtPrivateKey"),
    {
      expiresIn: "90d",
    }
  );
  return token;
};

const User = mongoose.model("User", userSchema);

const complexityOptions = {
  min: 8,
  max: 55,
  lowerCase: 1,
  upperCase: 1,
  numeric: 1,
  symbol: 1,
  requirementCount: 2,
};

function validateUser(user) {
  const schema = Joi.object({
    role: Joi.string(),
    name: Joi.string().min(3).max(55).required(),
    email: Joi.string().min(5).max(55).required().email(),
    password: passwordComplexity(complexityOptions),
  });
  return schema.validate(user);
}

exports.User = User;
exports.validate = validateUser;
