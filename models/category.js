// Loading Libraries
const Joi = require("joi");
const mongoose = require("mongoose");

// Defining the Categories Schema
const categorySchema = new mongoose.Schema(
  {
    priority: {
      type: Number,
      default: 1,
      min: 1,
    },
    name_de: {
      type: String,
      required: true,
      minlength: 3,
      maxlength: 100,
    },
    name_en: {
      type: String,
      required: true,
      minlength: 3,
      maxlength: 100,
    },
  },
  {
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at",
    },
  }
);

const Category = mongoose.model("Category", categorySchema);

// Category Validation
function validateCategory(category) {
  const schema = Joi.object({
    name_de: Joi.string().min(3).max(100).required(),
    name_en: Joi.string().min(3).max(100).required(),
    priority: Joi.number().integer(),
  });

  return schema.validate(category);
}

// Exporting the Module
exports.categorySchema = categorySchema;
exports.Category = Category;
exports.validate = validateCategory;
