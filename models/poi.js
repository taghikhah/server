// Loading Libraries
const Joi = require("joi");
const mongoose = require("mongoose");
const { categoryScheme } = require("./category");

// Defining the Pois Schema
const poiSchema = new mongoose.Schema(
  {
    priority: {
      type: Number,
      default: 1,
      min: 1,
    },
    category_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category",
    },
    title_de: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 100,
    },
    title_en: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 100,
    },
    description_de: {
      type: String,
      required: true,
      minlength: 50,
      maxlength: 5000,
    },
    description_en: {
      type: String,
      required: true,
      minlength: 50,
      maxlength: 5000,
    },
    caption_de: {
      type: String,
      trim: true,
      required: true,
      minlength: 5,
      maxlength: 500,
    },
    caption_en: {
      type: String,
      trim: true,
      required: true,
      minlength: 5,
      maxlength: 500,
    },
    add_info_url: {
      type: String,
      default: null,
      trim: true,
      lowercase: true,
      match: /(http(s?)):\/\//i,
    },
    is_displayed_on_table: { type: Boolean, require: true },
    has_histocache_location: { type: Boolean, require: true },
    lat: {
      type: Number,
      required: function () {
        return this.has_histocache_location;
      },
      min: 51.0,
      max: 51.1,
    },
    long: {
      type: Number,
      required: function () {
        return this.has_histocache_location;
      },
      min: 13.0,
      max: 13.9,
    },
    filename: {
      type: String,
      required: true,
      trim: true,
      lowercase: true,
    },
    image_aspect_ratio: {
      type: Number,
      required: true,
      min: 0.1,
      max: 4,
    },
    has_viewpoint_location: { type: Boolean, require: true },
    viewpoint_lat: {
      type: Number,
      required: function () {
        return this.has_viewpoint_location;
      },
      min: 51.0,
      max: 51.1,
    },
    viewpoint_long: {
      type: Number,
      required: function () {
        return this.has_viewpoint_location;
      },
      min: 13.0,
      max: 13.9,
    },
    viewpoint_filename: {
      type: String,
      required: function () {
        return this.has_viewpoint_location;
      },
      trim: true,
      lowercase: true,
    },
    viewpoint_image_aspect_ratio: {
      type: Number,
      required: function () {
        return this.has_viewpoint_location;
      },
      min: 0.1,
      max: 4,
    },
    viewpoint_image_height: {
      type: Number,
      required: function () {
        return this.has_viewpoint_location;
      },
      min: 1,
    },
    viewpoint_image_offset: {
      type: Number,
      required: function () {
        return this.has_viewpoint_location;
      },
      min: 1,
    },
    viewpoint_image_vertical_offset: {
      type: Number,
      required: function () {
        return this.has_viewpoint_location;
      },
    },
  },
  {
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at",
    },
  }
);

const Poi = mongoose.model("Poi", poiSchema);
//
// Poi Validation
function validatePoi(poi) {
  const schema = Joi.object({
    priority: Joi.number().integer(),
    category_id: Joi.objectId().required(),
    title_en: Joi.string().min(5).max(100).required(),
    title_de: Joi.string().min(5).max(100).required(),
    description_en: Joi.string().min(50).max(5000).required(),
    description_de: Joi.string().min(50).max(5000).required(),
    caption_en: Joi.string().min(5).max(500).required(),
    caption_de: Joi.string().min(5).max(500).required(),
    add_info_url: Joi.string().optional().allow(null).allow("").empty(""),

    is_displayed_on_table: Joi.boolean().required(),

    has_histocache_location: Joi.boolean().required(),
    lat: Joi.number()
      .min(51.0)
      .max(51.1)
      .when("has_histocache_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    long: Joi.number()
      .min(13.0)
      .max(13.9)
      .when("has_histocache_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    filename: Joi.string().when("has_histocache_location", {
      is: true,
      then: Joi.required(),
      otherwise: Joi.allow(null).allow("").empty(""),
    }),
    image_aspect_ratio: Joi.number()
      .min(0.1)
      .max(4)
      .when("has_histocache_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),

    has_viewpoint_location: Joi.boolean().required(),
    viewpoint_lat: Joi.number()
      .min(51.0)
      .max(51.1)
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    viewpoint_long: Joi.number()
      .min(13.0)
      .max(13.9)
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    viewpoint_filename: Joi.string().when("has_viewpoint_location", {
      is: true,
      then: Joi.required(),
      otherwise: Joi.allow(null).allow("").empty(""),
    }),
    viewpoint_image_aspect_ratio: Joi.number()
      .min(0.1)
      .max(4)
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    viewpoint_image_height: Joi.number()
      .min(1)
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    viewpoint_image_offset: Joi.number()
      .min(1)
      .when("has_viewpoint_location", {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }),
    viewpoint_image_vertical_offset: Joi.number().when(
      "has_viewpoint_location",
      {
        is: true,
        then: Joi.required(),
        otherwise: Joi.allow(null).allow("").empty(""),
      }
    ),
  });

  return schema.validate(poi);
}

// Exporting the Module
exports.poiSchema = poiSchema;
exports.Poi = Poi;
exports.validate = validatePoi;
