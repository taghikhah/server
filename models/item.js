// Loading Libraries
const mongoose = require("mongoose");
const Joi = require("joi");
const { poiScheme } = require("./poi");

// Database Schema
const itemSchema = new mongoose.Schema(
  {
    poi_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Poi",
    },
    priority: {
      type: Number,
      default: 1,
      min: 1,
    },
    description_de: {
      type: String,
      required: true,
      minlength: 10,
      maxlength: 5000,
    },
    description_en: {
      type: String,
      required: true,
      minlength: 10,
      maxlength: 5000,
    },
    caption_de: {
      type: String,
      trim: true,
      required: true,
      minlength: 5,
      maxlength: 500,
    },
    caption_en: {
      type: String,
      trim: true,
      required: true,
      minlength: 5,
      maxlength: 500,
    },
    filename: {
      type: String,
      trim: true,
      lowercase: true,
      required: true,
    },
    image_aspect_ratio: {
      type: Number,
      required: true,
      min: 0.1,
      max: 4,
    },
  },
  {
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at",
    },
  }
);

const Item = mongoose.model("Item", itemSchema);

// Items Validation
function validateItem(item) {
  const schema = Joi.object({
    poi_id: Joi.objectId().required(),
    priority: Joi.number().integer(),
    description_de: Joi.string().min(10).max(5000).required(),
    description_en: Joi.string().min(10).max(5000).required(),
    caption_de: Joi.string().min(5).max(500).required(),
    caption_en: Joi.string().min(5).max(500).required(),
    filename: Joi.string().required(),
    image_aspect_ratio: Joi.number().min(0.1).max(4).required(),
  });

  return schema.validate(item);
}

// Exporting the module
exports.itemSchema = itemSchema;
exports.Item = Item;
exports.validate = validateItem;
